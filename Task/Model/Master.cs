﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task.Model
{
    class Master
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Surname { get; set; }

        public List<Job> Jobs { get; set; }


    }
}
