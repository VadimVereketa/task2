﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task.Model
{
    class BusinessDay
    {
        public DateTime Date { get; set; }

        public List<Master> Masters { get; set; }

    }
}
