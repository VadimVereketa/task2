﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task.Model
{

    class Job
    {
        public String KindOfWork { get; set; }
        public Double Sum { get; set; }
        public Double Wash { get; set; }
        public Double Paint { get; set; }
        public Double AdditionalMaterials { get; set; }
        public Double Work { get; set; }
        public Double Salon { get; set; }
        public Double Master { get; set; }
    }
}
